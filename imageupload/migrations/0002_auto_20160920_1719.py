# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-20 17:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('imageupload', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='pub_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='image',
            name='uploadimage',
            field=models.ImageField(upload_to='Appsec_images/%Y/%m/%d'),
        ),
    ]
